require 'rails_helper'

describe Employee, type: :model do
	before(:context) do
  		@emp= FactoryGirl.build(:employee)
  		
  		@dept = FactoryGirl.build(:department)
  	end

	context "model associations and validation checks" do

  		it{should belong_to(:department)}

      it { should validate_presence_of(:email_id) }
  	end

 
  	  
  		
 

end
