FactoryGirl.define do
	factory :training do

		course {Faker::Name.name}
		Type "Foundation" 
		batchno 23
		trainer_id 453
		trainer_name "shaik"
		no_of_trainees 3
		venue "training room"
		status "created"
		created_at Time.now
		updated_at Time.now
		
	end

	factory :enrollment do

	employee_id {Faker::Number.between(400..600)}
	training_id "lorem psum123"
	cleared "NO"
	created_at Time.now
	updated_at Time.now
	end
	

end