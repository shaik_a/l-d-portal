FactoryGirl.define do
	factory :employee do

		name {Faker::Name.name}
		manager_id {Faker::Number.between(400,600)} 
		gender "female"
		date_of_joining "22-06-2015"
		experience 4
		email_id "super@gmail.com"
		designation "software engg."
		ExtensionNo 234
		created_at Time.now
		updated_at Time.now
		department
	end

	factory :department do
		
		name {Faker::Name.name}
		manager_id {Faker::Number.between(400,600)} 
		no_of_employees 5
		created_at Time.now
		updated_at Time.now
		 
		
	end

end