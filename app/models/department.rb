class Department < ActiveRecord::Base
	has_many :employees,dependent: :destroy
	
	validates_presence_of :manager_id, :name
end
