class ProjectReport < ActiveRecord::Base

belongs_to :training

#method for download option
def self.to_csv(options = {})
  CSV.generate(options) do |csv|
    csv << column_names
    all.each do |project|
      csv << project.attributes.values_at(*column_names)
    end
  end
end


#parameter based on which search can be done





#validations

validates_inclusion_of :gui, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :timeline, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :time_space, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :code_convention, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :logic, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :use_technology, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :tools_usage, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :presentation, :in => 1..4, :message => "can only be between 1 and 4." 


end


