class AssignmentReport < ActiveRecord::Base

	belongs_to :training
#validations for training
validates_inclusion_of :meeting_timelines, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :code_integrity, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :code_convention, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :code_optimization, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :logic, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :knowledge, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :format_submission, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :Time_space_effeciency, :in => 1..4, :message => "can only be between 1 and 4." 


#method for download option
	def self.to_csv(options={})
	CSV.generate(options) do |csv|
		csv << column_names
		all.each do|assignment|
			csv<<assignment.attributes.values_at(*column_names)
		end
	end
end
end
