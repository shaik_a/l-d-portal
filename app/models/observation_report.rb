class ObservationReport < ActiveRecord::Base
belongs_to :training


#method for download option
def self.to_csv(options = {})
  CSV.generate(options) do |csv|
    csv << column_names
    all.each do |observationReport|
      csv << observationReport.attributes.values_at(*column_names)
    end
  end
end

#parameter based on which search can be done

validates_inclusion_of :technical_knowledge, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :coding_skills, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :logical_thinking_skills, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :time_management, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :attendance, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :approach_towards_learning, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :attitude_and_body_language, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :written_communication, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :spoken_communication, :in => 1..4, :message => "can only be between 1 and 4." 
validates_inclusion_of :participation, :in => 1..4, :message => "can only be between 1 and 4." 
validates_presence_of :Remarks
end

