class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable
  has_many :scorecards
  has_one :employee
  belongs_to :role

  delegate :url_helpers, to: 'Rails.application.routes'
  
  def get_navigation_links
    case self.role.role
    when 'Admin'
      populate_admin_navigation_list
    when 'Trainer'
      populate_trainer_navigation_list
    when 'Manager'
      populate_manager_navigation_list
    end
  end

  validates :username, format: { with: /\A[a-zA-Z0-9]+\Z/ }, :presence => true,:uniqueness => {:allow_blank => true} 
  
  def email_required?
    false
  end

  private

  def populate_admin_navigation_list
    #structue is [id,link_name,url,parent], zero indicates root level links
    [
      [1,'Certifications','#','fa-file-text',0],
        [2,'Mock Certification Tests',url_helpers.certifications_path,'fa-address-card-o',1],
        [3,'Employee Certifications',url_helpers.empreports_path,'fa-address-card-o',1], 
      [4,'Employee Database','#','fa-database',0],
        [5,'Add an Employee','#','fa-database',4],
        [6,'View all Employees','#','fa-database',4],
      [7,'Tasks','#','fa-check-square-o',0], 
        [8,'Create Task',url_helpers.new_task_path,'fa-plus',7], 
        [9,'All Tasks',url_helpers.tasks_path,'fa-tasks',7], 
      [10,'Trainings','#','fa-graduation-cap',0], 
        [11,'All Trainings',url_helpers.trainings_path,'fa-list-alt',10],
        [12,'Schedule Training',url_helpers.new_training_path,'fa-calendar',10], 
      [13,'User Accounts','#','fa-users',0], 
        [14,'Create User',url_helpers.new_admin_path,'fa-user-plus',13],
        [15,'All Users',url_helpers.admins_path,'fa-user-plus',13],
      [15,'Score Cards','#','fa-file-text',0]

    ]
  end

  def populate_manager_navigation_list
    []
  end

  def populate_trainer_navigation_list
    [[1,'Certifications','',0][2,'Employee Database','',0],[3,'Tasks','',0],[4,'Trainings','',0],[5,'User Accounts','',0],[6,'Score Cards','',0],
    [7,'All Tasks',tasks_path,3],[8,'Create Task',new_task_path,3],[9,'All Trainings',trainings_path,4],
    [10,'Schedule Training',new_training_path,4],[11,'Mock Certification Tests',certifications_path,1],[12,'Employee Certifications',empreports_path,1]]
  end
      
end
