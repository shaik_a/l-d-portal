class Training < ActiveRecord::Base
	belongs_to :user

	has_many :trainee_enrollments,:dependent=>:destroy
	
	validates_presence_of :from ,:to
	validates_presence_of :course, :message=>"field can't be blank"

	scope :sorted,lambda {order("trainings.from DESC")}


	has_many :attendance_reports
	has_many :assignment_reports
	has_many :evaluation_reports

	
	has_many :observation_reports
	has_many :project_reports



	has_many :attendance_reports,:dependent=>:destroy
	has_many :assignment_reports,:dependent=>:destroy
	has_many :evaluation_reports,:dependent=>:destroy
	has_many :observation_reports,:dependent=>:destroy
	has_many :project_reports,:dependent=>:destroy

	def self.types
		['Foundation','Mandatory','Softskils','Functional','Technical']
	end


end

