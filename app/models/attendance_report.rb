class AttendanceReport < ActiveRecord::Base
belongs_to :training

   

#validates_uniqueness_of :date, scope: :training_id
#method for download
def self.to_csv(options = {})
  CSV.generate(options) do |csv|
    csv << column_names
    all.each do |attendance|
      csv << attendance.attributes.values_at(*column_names)
    end
  end
end

validates_presence_of :topic, :message => "must be present" 

#validations
validates_presence_of :trainee_name
validates_presence_of :presence
validates_presence_of :no_of_sessions

end
