class EvaluationReport < ActiveRecord::Base
belongs_to :training

# method for download options
def self.to_csv(options = {})
  CSV.generate(options) do |csv|
    csv << column_names
    all.each do |evaluation|
      csv << evaluation.attributes.values_at(*column_names)
    end
  end
end
#parameter based on which search can be done

validates :trainee_name, presence: true
validates :tests, presence: true
validates :marks_scored, presence: true
validates :max_marks,  presence: true


end

 


