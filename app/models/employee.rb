class Employee < ActiveRecord::Base
	belongs_to :department
	has_many :trainee_enrollments
	belongs_to :user
  has_one :reporting_manager, :class_name => 'Employee', :foreign_key => 'manager_id'


  scope :name_like, -> (name) { where("name like ?", name)}

#validations
	
	validates_presence_of :email_id ,:employee_id ,:name ,:date_of_joining

end
