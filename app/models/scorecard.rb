class Scorecard < ActiveRecord::Base

  belongs_to :user

  validates :training_needs, presence: true
  validates :employees_present_tnr, presence: true
  validates :tech_tnr_hrs, presence: true
  validates :soft_tnr_hrs, presence: true
  validates :emp_yet_to_attend, presence: true
  validates :Knowledge_sessions_conducted, presence: true
  validates :employees_present_kss, presence: true
  validates :certifications_done, presence: true
  validates :technology, presence: true
end
