class UserMailer < ApplicationMailer
#mail ID of the person from whose account , account creation mail will be sent
  default from: "vaiblucky@gmail.com"
  layout 'mailer'
  
   def welcome_email(user)
    @user = user
    @url  = 'http://10.10.1.241'
    #mail ID of the person to whom mail is sent
    mail(to: @user.email, subject: 'L&D portal:Login Crendentials')
   end

   def task_email(user)
    @user = user
    @url  = 'http://10.10.1.241'
    #mail ID of the person to whom mail is sent
    mail(to: @user.email, subject: 'L&D portal:Assigned Task')
   end

   def destroy_email(user)
    @user = user
    @url  = 'http://10.10.1.241'
    #mail ID of the person to whom mail is sent
    mail(to: @user.email, subject: 'L&D portal:Notification')
   end

   def training_email(user)
    @user = user
    @url  = 'http://10.10.1.241'
    #mail ID of the person to whom mail is sent
    mail(to: @user.email, subject: 'L&D portal:Notification')
   end
   
end
