module DeviseHelper
  def devise_error_messages!
    return "" if resource.errors.empty?

     

    render(:partial => 'shared/errors',
    :locals => {:object => resource})

  end

  def devise_error_messages?
    resource.errors.empty? ? false : true
  end

end