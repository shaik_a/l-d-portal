module ApplicationHelper

  #generate navigation links

  def get_navigation_tree
    current_user.get_navigation_links
  end
  #method to check first time login


  #A method to display error partial in app/shared folder
  def error_messages_for(object)
    render(:partial => 'shared/errors',:locals => {:object => object})
  end

  #A method for date format conversion :used in trainings/index.html.erb
  def date_format(training)
    first=training.from
    last=training.to
  #changing the format of the date using proc
    @dates=(first..last).collect
    @format= Proc.new do |n| n.strftime("%d/%b/%Y")
    end
    @list=@dates.collect(&@format)
  end

  #A method to select trainees under a particular training :used in all L&D reports new.html.erb
  def trainee_list(training)
    @traineelist=TraineeEnrollment.where("training_id=?",training.id) 
  end

  # Fetches facilitator names in attendances reports
  def facilitator
    @u_id=User.find(current_user.id).employee_id
    @user=Employee.find(@u_id).name
  end

  #Fetches accounts of trainers, :used in trainings/new.html.erb and tasks/new.html.erb
  def trainerlist
    @employees=User.where("type=?","Trainer")
  end

  # Type conversion for of rating to percentage for observation and project report

  def weightage(value)
    if value==1
      return percentage="25%"
    elsif value==2
      return percentage="50%"
    elsif value==3
      return percentage="75%"
    else
      return percentage="100%"
    end
  end
end
