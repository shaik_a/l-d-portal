class DepartmentsController < ApplicationController
#before_action :authenticate_login!


  def index
    @department=Department.order("id ASC")
  end


  #displays the details of a particular department
  def show
    @department=Department.find(params[:id])
  end

  def create 
    #instantiate  a new object
    #if save succeds redirect to the index action 
    #if save  fails redisplay the form 
    @department=Department.new(department_params)

    @employees=Employee.pluck(:employee_id)
    valid=false
    
    if @employees.include?(@department.manager_id)
     valid=true
    end
    
    if valid and @department.save
      redirect_to departments_path
    else
       #if create fails redirecting to the same page      
       render('new')
       flash[:notice]="wrong manager id"
    end
  end


  def new
    @department=Department.new()
    gon.employees= Employee.pluck(:name)
    gon.id= Employee.pluck(:employee_id)
  end

  def edit
    @department=Department.find(params[:id])
  end

   def update
      @department=Department.find(params[:id])
      @employee=Employee.all.collect(&:employee_id)
      valid=false
   
      @employee.each do |emp|
        if emp==@department.manager_id
          valid=true    
        end  
      end

      if valid and @department.update_attributes(department_params)
        redirect_to departments_path
      else
        #if update fails redirecting to the same page
        flash[:notice]="wrong manager id"
        render('edit')
     end
    end

  #doesnt delete it but gives the confirmation for deleting
  def delete
    @department=Department.find(params[:id])
  end

  #deletes the department from database for selected id 
  def destroy
    @department=Department.find(params[:id]).destroy
    redirect_to(:action=>'index')
  end

  private
    #whitelisting the attributes
    def department_params
      params.require(:department).permit(:name,:id,:manager_id)
    end
end

