class EmployeesController < ApplicationController



  before_action :authenticate_user!

  def index

    if params[:search].present?
      @search =Employee.search do
        fulltext params[:search]
        paginate :page => params[:page],:per_page=>8
      end
      @employees=@search.results
    else
      @employees = Employee.where("name!=?",'Admin').paginate(:page =>params[:page],:per_page=>8).order("name ASC")
    end   
  end

  #displays the details of a particular employee with his manager ID and department name
  def show
    @employee=Employee.find(params[:id])
  begin
    @department=Department.find(@employee.department_id).name 
  rescue ActiveRecord::RecordNotFound  
    @department="not Assigned"
  end
  begin
    @manager=Employee.find(@employee.manager_id)
  rescue ActiveRecord::RecordNotFound  
    @manager=Employee.find(@employee.employee_id)
    @manager.name="Not Assigned"
  end
end

def create 
  #instantiate  a new object
  #if save succeds redirect to the index action 
  #if save  fails redisplay the form 
  @department=Department.pluck(:id)
  @employee=Employee.new(employee_params)
  if @employee.save
    redirect_to employees_path
  else
    #if create fails redirecting to the same page
    render('new')
  end     
end


def new
  @department=Department.pluck(:id)
  @employee=Employee.new()

  
end

  #edits the record for employees
  def edit
    @department=Department.pluck(:id)
    @employee=Employee.find(params[:id])
  end

  #updates the record for employees
  def update
    @department=Department.pluck(:id)
    @employee=Employee.find(params[:id])
    if @employee.update_attributes(employee_params)
      redirect_to(:action=>'index')
    else
      #if update fails redirecting to the same page
      render('new')
    end
  end

  #doesnt delete it but gives the confirmation for deleting
  def delete
    @employee=Employee.find(params[:id])
  end

  #deletes the employee from database for selected id 
  def destroy
    employee=Employee.find(params[:id]).destroy
    redirect_to(:action=>'index')
  end

  private

    def employee_params
      params.require(:employee).permit(:name,:employee_id,:status,:location,:date_of_joining,:department_id,:email_id)
    end
end


