class UsersController < ApplicationController

  before_action :authenticate_user!

  before_action :first_time, only:[:index]

  def index
    
    #method to check if first time login and send the user to password reset

    
    redirect_to trainings_path  
  end

  def first
    @user=User.find(current_user.id)
  end

  def update
    @user=User.find(params[:id])
    if @user.update_attributes(user_params)
      sign_out current_user
      flash[:notice]="Password has been Reset,Login with your new password"
      redirect_to(user_session_path)
    else
      #if update fails redirecting to the same page
      render('first')
    end
  end

  private

  def first_time
    @count= User.find(current_user.id).sign_in_count

    if @count == 1
      redirect_to first_users_path
    end
  end

  def user_params
    params.require(:user).permit(:password,:password_confirmation)
  end
end



