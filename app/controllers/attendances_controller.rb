class AttendancesController < ApplicationController
	
  before_action :find_training

  require 'will_paginate/array'


  def index
    #lists all the attendance reports

    @tnrenrolls=TraineeEnrollment.all

    #current logged in user's emp id
    @user=User.find(current_user.id).employee_id

    #fetches list of employees under logged user 
    @Emp=Employee.where("manager_id=?",@user)

    @excelexport=AttendanceReport.where("training_id=?",@training.id).order("date ASC")


  
  
   
    #if search is used
    if params[:search].present?
      @search =AttendanceReport.where("training_id=?",@training.id).order("date DESC").search do
      fulltext params[:search]
  
      paginate :page => params[:page],:per_page=>4
      end
      @attendances=@search.results
    else
      @attendances = AttendanceReport.where("training_id=?",@training.id).order("date DESC").paginate(:page =>params[:page],:per_page=>4)
    end   


    respond_to do |format|

    format.html
    format.xls # { send_data @products.to_csv(col_sep: "\t") }
  end
end


  def edit
    @attendance=AttendanceReport.find(params[:id])
  end

  def update
    @attendance=AttendanceReport.find(params[:id])
    if @attendance.update_attributes(attendance_params)
      redirect_to(attendances_path(:id=>@attendance.id,:training_id=>@training.id))
    else
      #if update fails redirecting to the same page
      render('edit')
    end
 end

  #for the confirmation
  def delete
    @attendance=AttendanceReport.find(params[:id])
  end

  #actual deleting it from database
  def destroy
    @attendance=AttendanceReport.find(params[:id]).destroy
    flash[:notice]= "Attendence ID #{@attendance} removed"
    redirect_to(attendances_path(:training_id=>@training.id))
  end

  def new
    @attendance=AttendanceReport.new   
  end

  def create
    @attendance=AttendanceReport.new(attendance_params)
    if @attendance.save
      redirect_to attendances_path({:training_id=>@training.id})
    else
    #if create fails render to the same page
      render ('new')
    end
  end



  
 private


  #whitelisting the attributes
  def attendance_params
    params.require(:attendance).permit(:date,:training_id,:topic,:trainee_name,:presence,:no_of_sessions,:facilitator)
  end

  def find_training  
    @training=Training.find(params[:training_id])
  end
end

