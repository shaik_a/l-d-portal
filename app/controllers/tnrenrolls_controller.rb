class TnrenrollsController < ApplicationController
  
  before_action :find_training



  def new
    @tnrenroll=TraineeEnrollment.new
    gon.employees= Employee.pluck(:name)
    gon.id= Employee.pluck(:employee_id)
  end


  # adding trainees for a particular training 
  def create
  	@tnrenroll=TraineeEnrollment.new(tnr_params)
    @employee=Employee.all.collect(&:employee_id)
  	valid=false
    @employee.each do |emp|
      if emp==@tnrenroll.employee_id
        valid=true    
      end  
    end
    
    if valid and @tnrenroll.save

      @employee_name=Employee.find(@tnrenroll.employee_id).name
      @tnrenroll.trainee_name=@employee_name
      @tnrenroll.save 
  		
    	redirect_to tnrenrolls_path({:training_id=>@training.id})
  	else
  		flash[:notice]="trainee ID #{@tnrenroll.employee_id} is invalid" 
      render ('new')
    end
  end  

  def index
  	@tnrenrolls=TraineeEnrollment.all.order(trainee_name: :ASC)
 	
    #current logged in user's emp id
    @user=User.find(current_user.id).employee_id

    #dept of current logged in user
    @Lob_dept_id=Employee.find(@user).department_id

    #all employees
    @employees=Employee.all

    #fetches list of employees under logged user 
    @Emp=Employee.where("manager_id=?",@user)
  end

 
  def edit
    @tnrenroll=TraineeEnrollment.find(params[:id])
  end

  def update
    #flash[:notice]="trainee details updated successfully"
    @trainee=TraineeEnrollment.find(params[:id])

    if @trainee.update_attributes(tnr_params)
      redirect_to(:action=>'index',:training_id=>@training.id)
    else
      #if update fails redirecting to the same page
      render('edit') 
    end
  end

  def delete
    @tnrenroll=TraineeEnrollment.find(params[:id])
  end
 
  #deletes the training from database for selected id  
  def destroy

    @tnrenroll=TraineeEnrollment.find(params[:id]).destroy

    redirect_to(tnrenrolls_path(:training_id=>@training.id))

  end

  #methods under private are no longer actions
  private
    #whitelisting the atrributes for training enrollments
    def tnr_params
      params.require(:tnrenroll).permit(:employee_id,:trainee_name,:training_id,:cleared)
    end

    def find_training
      @training=Training.find(params[:training_id])
    end
end
