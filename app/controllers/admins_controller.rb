class AdminsController < ApplicationController
  #authenticates for the valid admin
  before_action :checkadmin, except: [:show]

  before_action :authenticate_user!
  #before_action :validate_user

  def index

  end

  def new
    @user=User.new
    gon.employees= Employee.pluck(:name)
    gon.id= Employee.pluck(:employee_id)
  end

  #admin is able to add users by providing a valid employee id that has to be presesnt 
  #in employee table and mail will be sent to the user along with his creadentials.

  def create
    #checks whether entered employee ID is valid
    @user=User.new(user_params)

    if @user.save 
      redirect_to admin_path(:id)
      UserMailer.welcome_email(@user).deliver
    else
      render new_admin_path        
    end
  end

  def delete
    @user= User.find(params[:id])
  end

  def destroy
    @user= User.find(params[:id])
    @user.destroy
    flash[:notice] = "User #{@user.role} removed successfully."
    redirect_to(:action => 'show')
    #email is sent when account is deleted
    UserMailer.destroy_email(@user).deliver
  end

  def show
    #currently registered users
    @users=User.all.order("username ASC")
  end

  private

  def user_params
    params.require(:user).permit(:employee_id,:username,:email,:type,:password,:password_confirmation)
  end


  def checkadmin
    if User.find(current_user.id).role=="Admin"

    else 
      flash[:notice] = "illegal access attempted, auto logout"
      sign_out current_user
      redirect_to(user_session_path)
    end
  end


end
