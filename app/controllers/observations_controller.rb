class ObservationsController < ApplicationController
    before_action :find_training

	def index
  #pagination with search bar and download option
  @excelexport=ObservationReport.where("training_id=?",@training.id).order("trainee_name ASC")
  @observations=ObservationReport.all

    if params[:search].present?
      @search =ObservationReport.where("training_id=?",@training.id).order("trainee_name DESC").search do
        fulltext params[:search]
        paginate :page => params[:page],:per_page=>4
      end
      @observations=@search.results
    else
      @observations= ObservationReport.where("training_id=?",@training.id).order("trainee_name DESC").paginate(:page =>params[:page],:per_page=>4)
    end   

    #current logged in user's emp id
    @user=User.find(current_user.id).employee_id

    #fetches list of employees under logged user 
    @Emp=Employee.where("manager_id=?",@user)
  
    respond_to do |format|
      format.html
      format.csv { send_data   @observations.to_csv }
      format.xls # { send_data @products.to_csv(col_sep: "\t") }
    end
  end


  #details of the observation reports
  def show
    @observation=ObservationReport.find(params[:id])
  end

  def new
    @observation=ObservationReport.new
    @traineelist=TraineeEnrollment.where("training_id=?",@training.id) 
  end



  #creating the observation report
  def create
    @observation=ObservationReport.new(observation_params)
    if @observation.save
      redirect_to observations_path({:training_id=>@training.id})
    else
      #if create fails redirecting to the same page
      render('new')
    end
  end


  #edits the attributes for observation report
  def edit
    @observation=ObservationReport.find(params[:id])
  end


  #updates the attributes for observation report
  def update
    @observation=ObservationReport.find(params[:id])
    if @observation.update_attributes(observation_params)
      redirect_to(observations_path(:id=>@observation.id,:training_id=>@training.id))
    else
      #if update fails redirecting to the same page
      render('edit')
    end
 end

  #doesnt delete it but gives the confirmation for deleting
  def delete
    @observation=ObservationReport.find(params[:id])
  end

  #deletes the observation report from database for selected id 
  def destroy
    @observation=ObservationReport.find(params[:id]).destroy
    redirect_to(observations_path(:training_id=>@training.id))
  end

  private

    def observation_params
      params.require(:observation).permit(:trainee_name,:training_id,:technical_knowledge,:coding_skills,:coding_skills,:logical_thinking_skills,:time_management,:attendance,:approach_towards_learning,:attitude_and_body_language,:written_communication,:spoken_communication,:participation,:Remarks)
    end

    def find_training
      @training=Training.find(params[:training_id])
    end
end














