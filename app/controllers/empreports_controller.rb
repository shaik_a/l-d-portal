class EmpreportsController < ApplicationController

  def index
    
    #lists all the mock certification test reports
    @excelexport=EmployeeReport.all

    if params[:search].present?

      @search =EmployeeReport.all.search do
        fulltext params[:search]
        paginate :page => params[:page],:per_page=>4
      end
      @empreports=@search.results
    else
      @empreports = EmployeeReport.all.paginate(:page =>params[:page],:per_page=>4)
    end   

    #current logged in user's emp id
    @user=User.find(current_user.id).employee_id

    #fetches list of employees under logged user 
    @Emp=Employee.where("manager_id=?",@user)

    respond_to do |format|
      format.html
      format.csv { send_data   @certification.to_csv }
      format.xls # { send_data @products.to_csv(col_sep: "\t") }
    end  
  end

 

  #shows the details of employee report
  def show
    @empreport=EmployeeReport.find(params[:id])
  end

  def new
    @empreport=EmployeeReport.new  
    gon.employees= Employee.pluck(:name)
    gon.id= Employee.pluck(:employee_id)
  end
  
  #creates the record
  def create
    @empreport=EmployeeReport.new(empreport_params)
    if @empreport.save

      @employee_name=Employee.find(@empreport.employee_id).name
      @empreport.employee_name=@employee_name
      @empreport.save

      redirect_to empreports_path
    else
      render('new')
    end
  end

  #edits the attributes for employee reports
  def edit
    @empreport=EmployeeReport.find(params[:id])
  end
  
  #updates the attributes for employee report
  def update
    @empreport=EmployeeReport.find(params[:id])
    if @empreport.update_attributes(empreport_params)
     
      @employee_name=Employee.find(@empreport.employee_id).name
      @empreport.employee_name=@employee_name
      @empreport.update_attributes(empreport_params)

      redirect_to empreports_path(:id=>@empreport.id)
    else
      render ("edit")
    end
  end

  #doesnt delete it but gives the confirmation for deleting
  def delete
    @empreport=EmployeeReport.find(params[:id]) 
  end

  #deletes the empreports from database for selected id 
  def destroy
    @empreport=EmployeeReport.find(params[:id]).destroy
    redirect_to empreports_path
  end

  private

    def empreport_params
      params.require(:empreport).permit(:employee_id,:employee_name,:Certificate_Name,:Certification_code,:Team,:total_marks,:Marks_Scored,:Date_of_completion,:Amount,:Commitment_Period,:status)
    end
end


