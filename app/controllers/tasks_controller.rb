class TasksController < ApplicationController
 

  def index
    #selects all the tasks along with invoking pagination
    @tasks= Task.paginate(:page => params[:page], :per_page => 3)
  end

  def show
    @task=Task.find(params[:id])
  end

  def new  
    @task=Task.new
    @employees=User.where("type=?","Trainer")
  end

  #creating new task for the trainer
  def create
    @task=Task.new(task_params)

    @employee_name=Employee.find(@task.assignee_id).name
    @task.assignee=@employee_name


    @user=User.where("employee_id=?",@task.assignee_id).first

      if @task.save
        
        UserMailer.task_email(@user).deliver
        redirect_to tasks_path #redirects to index
      else
        #if update fails redirecting to the same page
      render('new')
    end
  end


  #edits the task attributes
  def edit
    @task=Task.find(params[:id])
  end

  #updates the task attributes
  def update
    @task=Task.find(params[:id])
    if @task.update_attributes(task_params) #whitelisting the values

      @employee_name=Employee.find(@task.assignee_id).name
      @task.assignee=@employee_name
      @task.update_attributes(task_params)
      
      redirect_to tasks_path #redirects to index
    else
    #if update fails redirecting to the same page
      render('edit')
    end
end

  #doesnt delete it but gives the confirmation for deleting
  def delete
    @task=Task.find(params[:id])
  end

  #deletes the task from database for selected id 
  def destroy
    @task=Task.find(params[:id]).destroy
    redirect_to tasks_path(:id=>@task.id)  
  end

  private

    def task_params
      params.require(:task).permit(:assignee_id,:assignee,:task_title,:description,:from,:to,:priority,:status)
    end
end

