class ProjectsController < ApplicationController
  before_action :find_training

	def index
    #pagination with search and download options
    @projects=ProjectReport.all
    @excelexport=ProjectReport.where("training_id=?",@training.id).order("trainee_name ASC")
  
    @gui=Score.pluck(:gui).first
    @timeline=Score.pluck(:timeline).first
    @time_space=Score.pluck(:time_space).first
    @logic=Score.pluck(:logic).first
    @code_convention=Score.pluck(:code_convention).first
    @use_technology=Score.pluck(:use_technology).first
    @tools_usage=Score.pluck(:tools_usage).first
    @presentation=Score.pluck(:presentation).first
   
       
    if params[:search].present?
      @search =ProjectReport.where("training_id=?",@training.id).order("trainee_name DESC").search do
        fulltext params[:search]
        paginate :page => params[:page],:per_page=>3
      end
      @projects=@search.results
    else
    @projects= ProjectReport.where("training_id=?",@training.id).order("trainee_name DESC").paginate(:page =>params[:page],:per_page=>3)
  end   

  #current logged in user's emp id
  @user=User.find(current_user.id).employee_id

  #fetches list of employees under logged user 
  @Emp=Employee.where("manager_id=?",@user)

    respond_to do |format|
      format.html
      format.csv { send_data  @projects.to_csv }
      format.xls # { send_data @products.to_csv(col_sep: "\t") }
    end
  end

  def show
    @project=ProjectReport.find(params[:id])
  end

  def new
    @project=ProjectReport.new
    @traineelist=TraineeEnrollment.where("training_id=?",@training.id) 
  end

def create
    @project=ProjectReport.new(project_params)                 
    if @project.save
      redirect_to projects_path({:training_id=>@training.id})
    else
      #if create fails redirecting to the same page
      render('new')
    end
  end

  #edits the attributes
  def edit
    @project=ProjectReport.find(params[:id])
  end

  def update
    @project=ProjectReport.find(params[:id])                           
    if @project.update_attributes(project_params)
      redirect_to projects_path ({:id=>@project.id,:training_id=>@training.id})
    else
      #if update fails redirecting to the same page
      render('edit')
    end
  end

  #doesnt delete it but gives the confirmation for deleting
  def delete
    @project=ProjectReport.find(params[:id])
  end


  def destroy
    @project=ProjectReport.find(params[:id])
    @project.destroy
    redirect_to(projects_path(:training_id=>@training.id))
  end

  private

    def project_params
      params.require(:project).permit(:trainee_name,:gui,:timeline,:time_space,:logic,:code_convention,:use_technology,:tools_usage,:presentation,:marks_scored,:training_id)
    end

    def find_training
      @training=Training.find(params[:training_id])
    end
end


 
  
   
