class CertificationsController < ApplicationController

      def index
        #lists all the mock certification test reports
      @excelexport=Certification.all

    if params[:search].present?
      @search =Certification.all.search do
      fulltext params[:search]
      paginate :page => params[:page],:per_page=>4
      end
      @certification=@search.results
    else
      @certification = Certification.all.paginate(:page =>params[:page],:per_page=>4)
    end   

    #current logged in user's emp id
    @user=User.find(current_user.id).employee_id

    #fetches list of employees under logged user 
    @Emp=Employee.where("manager_id=?",@user)

    respond_to do |format|
      format.html
      format.csv { send_data   @certification.to_csv }
      format.xls
    end
  end
  
  #displays the details of particular certification
  def show
    @certification=Certification.find(params[:id])
  end

  def new
    @certification=Certification.new
    gon.employees= Employee.pluck(:name)
    gon.id= Employee.pluck(:employee_id)
  end


  #creating a certification
  def create
    @certification=Certification.new(attendance_params)
  
    if @certification.save
      @employee_name=Employee.find(@certification.employee_id).name
      @certification.employee_name=@employee_name
      @certification.save
    
       redirect_to certifications_path
    else
      #if create fails redirecting to the same page
      render('new')
    end
  end


  #edits the record
  def edit
    @certification=Certification.find(params[:id])
  end
  #updates the record
  def update
    @certification=Certification.find(params[:id])

    if @certification.update_attributes(attendance_params)
      @employee_name=Employee.find(@certification.employee_id).name
      @certification.employee_name=@employee_name
   
      @certification.update_attributes(attendance_params)
   
      redirect_to certifications_path ({:id=>@certification.id})
    else
     #if update fails redirecting to the same page
      render('edit')
    end
 end

 #doesnt delete it but gives the confirmation for deleting
  def delete
    @certification=Certification.find(params[:id])
  end

#deletes the certification from database for selected id 
  def destroy
    @certification1=Certification.find(params[:id]).Test
    @certification=Certification.find(params[:id]).destroy
    flash[:notice]= "certification Entry removed"
    redirect_to(certifications_path)
  end
  #whitelisting the attributes
  def attendance_params
    params.require(:certification).permit(:employee_id,:employee_name,:Test,:Date,:Total_Score,:Score_achieved,:Remarks)
  end
end


