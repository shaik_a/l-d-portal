class AssignmentsController < ApplicationController
  before_action :find_training
    

  # adds pagination, search bar and download option for assignment reports   
  

  def index

    #current logged in user's emp id
    @user=User.find(current_user.id).employee_id

    #fetches list of employees under logged user 
    @Emp=Employee.where("manager_id=?",@user)

    #variable for excel export
    @excelexport=AssignmentReport.where("training_id=?",@training.id).order("trainee_name ASC")

    #total marks of assignments are made changeable and are stored in scores model
    @code_convention=Score.pluck(:code_convention).first
    @code_optimization=Score.pluck(:code_optimization).first
    @Time_space_effeciency=Score.pluck(:Time_space_effeciency).first
    @logic=Score.pluck(:logic).first 
    @meeting_timelines=Score.pluck(:meeting_timelines).first
    @code_integrity=Score.pluck(:code_integrity).first
    @knowledge=Score.pluck(:knowledge).first 
    @format_submission =Score.pluck(:format_submission).first

  
    #if search is used
    if params[:search].present?
      #lists assignments within that training only
      @search =AssignmentReport.where("training_id=?",@training.id).order("trainee_name DESC").search do
      fulltext params[:search]
      paginate :page => params[:page],:per_page=>3
      end
      @assignments=@search.results
    else
      @assignments = AssignmentReport.where("training_id=?",@training.id).order("trainee_name DESC").paginate(:page =>params[:page],:per_page=>3)
    end
  
    respond_to do |format|
      format.html
      format.csv { send_data  @assignments.to_csv }
      format.xls #{ send_data @products.to_csv(col_sep: "\t") }
    end
  end


  def show
    
  end

  def new
    @assignment=AssignmentReport.new
    @traineelist=TraineeEnrollment.where("training_id=?",@training.id)
  end

  def create
    @assignment=AssignmentReport.new(assignment_params)
    if @assignment.save
      redirect_to assignments_path({:id=>@assignment.id,:training_id=>@training.id})
    else
      #if create fails redirecting to the same page
      render('new')
    end
  end



  def edit
    @assignment=AssignmentReport.find(params[:id])
  end

  def update
    @assignment=AssignmentReport.find(params[:id])
    if @assignment.update_attributes(assignment_params)
      redirect_to assignments_path ({:id=>@assignment.id,:training_id=>@training.id}) 
    else
    #if update fails redirecting to the same page
      render('edit')
    end
 end

  def delete
    @assignment=AssignmentReport.find(params[:id])
  end


  def destroy
    @assignment=AssignmentReport.find(params[:id]).destroy
    redirect_to assignments_path({:id=>@assignment.id,:training_id=>@training.id})  
  end

  private


    #whitelisting the attributes
    def assignment_params
      params.require(:assignment).permit(:trainee_name,:training_id,:code_convention,:code_optimization,:Time_space_effeciency,:logic,:meeting_timelines,:code_integrity,:knowledge,:format_submission)
    end

    def find_training
      @training=Training.find(params[:training_id])
    end
end

