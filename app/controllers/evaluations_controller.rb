class EvaluationsController < ApplicationController
  before_action :find_training
 

      def index
        #instance excel export
          @excelexport=EvaluationReport.where("training_id=?",@training.id).order("date_taken ASC")
    


    if params[:search].present?
      @search =EvaluationReport.where("training_id=?",@training.id).order("date_taken DESC").search do
        fulltext params[:search]
        paginate :page => params[:page],:per_page=>4
      end
      @evaluations=@search.results
    else
      @evaluations = EvaluationReport.where("training_id=?",@training.id).order("date_taken DESC").paginate(:page =>params[:page],:per_page=>4)
    end   

    #current logged in user's emp id
    @user=User.find(current_user.id).employee_id

    #fetches list of employees under logged user 
    @Emp=Employee.where("manager_id=?",@user)
  
    respond_to do |format|
      format.html
      format.csv { send_data  @evaluations.to_csv }
      format.xls # { send_data @products.to_csv(col_sep: "\t") }
    end
  end
  
  #shows the details of evaluation report 
  def show
    @evaluation=EvaluationReport.find(params[:id])
  end

  def new
    @evaluation=EvaluationReport.new
    @traineelist=TraineeEnrollment.where("training_id=?",@training.id) 
  end
  
  #creating the evaluation report
  def create
    @evaluation=EvaluationReport.new(evaluation_params)
    @traineelist=TraineeEnrollment.where("training_id=?",@training.id)
    
    if @evaluation.save
      redirect_to evaluations_path({:training_id=>@training.id})
    else
      #if create fails redirecting to the same page
      render('new')
    end
  end

  #edits the attributes for observation report
  def edit
    @evaluation=EvaluationReport.find(params[:id])
  end

  #updates the attributes for observation report
  def update
    @evaluation=EvaluationReport.find(params[:id])
    if @evaluation.update_attributes(evaluation_params)

    redirect_to evaluations_path ({:id=>@evaluation.id,:training_id=>@training.id})
    else
      #if update fails redirecting to the same page
      render('edit')
    end
 end

  #doesnt delete it but gives the confirmation for deleting
  def delete
    @evaluation=EvaluationReport.find(params[:id])
  end

  #deletes the evaluation report from database for selected id 
  def destroy
    @evaluation=EvaluationReport.find(params[:id]).destroy
   
    redirect_to(evaluations_path(:training_id=>@training.id))
  end

  private

    def evaluation_params
      params.require(:evaluation).permit(:tests,:trainee_name,:date_taken,:marks_scored,:max_marks,:percentage,:training_id)
    end

   def find_training
      @training=Training.find(params[:training_id])
    end
end

