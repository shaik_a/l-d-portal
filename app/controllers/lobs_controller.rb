class LobsController < ApplicationController


	def destroy
		sign_out current_user
		redirect_to(user_session_path)
		flash[:notice]="logged out successfully"
	end
end
