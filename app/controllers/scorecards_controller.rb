class ScorecardsController < ApplicationController
  

  def index
  #lists the scorecard along with the pagination
    if params[:year].present?  
      @scorecards= Scorecard.where("year=? AND quater=?",params[:year],params[:quater])
    else
      @year=Time.now.strftime("%Y")
      getquater
      @scorecards= Scorecard.where("year=? AND quater=?",@year,@quater)
    end
  end

  def new
  	@scorecard=Scorecard.new
  end

  #creating the scorecard
  def create
  	@scorecard=Scorecard.new(scorecard_params)
  	 if @scorecard.save
        redirect_to scorecards_path
     else
        #if create fails redirecting to the same page
        render('new')
        flash[:notice]="wrong scorecard id"
     end
  end

  def edit
    @scorecard=Scorecard.find(params[:id])
  end

  def show
    @scorecard=Scorecard.find(params[:id])
  end

  def update
    @scorecard=Scorecard.find(params[:id])
      if @scorecard.update_attributes(scorecard_params)
      redirect_to(scorecards_path(:id=>@scorecard.id))
    else
      #if update fails redirecting to the same page
     render('edit') 
    end
  end

  #doesnt delete it but gives the confirmation for deleting
  def delete
    @scorecard=Scorecard.find(params[:id])
  end
  
  #deletes the scorecard from database for selected id 
  def destroy
    @scorecard=Scorecard.find(params[:id]).destroy
    redirect_to(scorecards_path(:id=>@scorecard.id))
  end



  private


    def getquater
      if Time.now.strftime("%-m")== 1 || Time.now.strftime("%-m")== 2 || Time.now.strftime("%-m")== 3
        @quater=1;
      elsif Time.now.strftime("%-m")== 4 || Time.now.strftime("%-m")== 5 || Time.now.strftime("%-m")== 6
        @quater=2;
      elsif Time.now.strftime("%-m")== 7 || Time.now.strftime("%-m")== 8 || Time.now.strftime("%-m")== 9
        @quater=3;
      else 
        @quater=4;
      end
    end

  def scorecard_params
    params.require(:scorecard).permit(:year,:employees_total,:quater,:training_needs,:employees_present_tnr,:tech_tnr_hrs,:soft_tnr_hrs,:emp_yet_to_attend,:Knowledge_sessions_conducted,:employees_present_kss,:certifications_done,:technology)
  end
end   