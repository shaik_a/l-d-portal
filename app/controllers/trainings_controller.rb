class TrainingsController < ApplicationController


  def index

    @trainings= Training.paginate(:page => params[:page],:per_page => 6).order(from: :DESC)


    #current logged in user's emp id
    @user=User.find(current_user.id).employee_id
    @users=User.all



    #all trainings
    @tnrenrolls=TraineeEnrollment.all

    #all employees
    @employees=Employee.all

    #all departments
    @depts=Department.all

    #fetches all employees under the current logged user
    @emp=Employee.where("manager_id=?",@user)
  end


  

  def new
    @training=Training.new 
  end

  #creates training
  def create
    @training=Training.new(training_params)
    @user=User.where("employee_id=?",@training.trainer_id).first
     
    if @training.save

      @employee_name=Employee.find(@training.trainer_id).name
      @training.trainer_name=@employee_name

      @training.save

      UserMailer.training_email(@user).deliver
       redirect_to trainings_path
     else
       render('new')
     end
  end

  def edit
    @training=Training.find(params[:id])
  end


  #updates training
  def update
    flash[:notice]="training updated successfully"
    @training=Training.find(params[:id])
    if @training.update_attributes(training_params)
      redirect_to(trainings_path(:id=>@training.id))
    else
      #if update fails redirecting to the same page
      render('edit') 
    end
 end

  #doesnt delete it but gives the confirmation for deleting
  def delete
    @training=Training.find(params[:id])
  end
 
  #deletes the training from database for selected id  
  def destroy

    @training=Training.find(params[:id]).destroy
    redirect_to(:action=>'index')
  end
  
  #all methods under private are not actions anymore.
  private
  
  #whitelisting the attributes for trainings 
  def training_params
    params.require(:training).permit(:id,:course,:Type,:batchno,:trainer_id,:no_of_trainees,:trainees,:venue,:status,:from,:to)
 end

 def find_training
    @training=Training.find(params[:training_id])
  end
end

