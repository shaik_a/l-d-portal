class CreateAssignmentReports < ActiveRecord::Migration
  def up
    create_table :assignment_reports do |t|
      t.float "training_id"
      t.string "trainee_name"
      t.float "Time_space_effeciency"
      t.float "code_convention"
    	t.float "code_optimization"
    	t.float "logic"
    	t.float "meeting_timelines"
    	t.float "code_integrity"
    	t.float "knowledge"
    	t.float "format_submission"

      t.timestamps null: false
    end
  end
   def down
  	drop_table :assignment_reports
  end
end
