class TrainingsReports < ActiveRecord::Migration
  def up
  create_table :reports_trainings, {:id=>false} do |t|

    
      t.integer "trainee_enrollement_id"
      t.integer "attendance_report_id"
      t.integer "observation_report_id"
      t.integer "evaluation_report_id"
      t.integer "assignment_report_id"
      t.integer "project_report_id"
      t.integer "employee_report_id"
      t.integer "certification_id"
    end
    add_index :reports_trainings, ["trainee_enrollement_id","attendance_report_id","observation_report_id","evaluation_report_id","assignment_report_id","project_report_id","employee_report_id","certification_id"], :name=>"trainee_report_index"
  end
  
  def down
  drop_table :reports_trainings
  end
end
