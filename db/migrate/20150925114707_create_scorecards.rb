class CreateScorecards < ActiveRecord::Migration
  def up
    create_table :scorecards do |t|

      t.string  "training_needs"
      t.string    "employees_present_tnr"
      t.integer    "employees_total"
      t.float    "tech_tnr_hrs"
      t.float    "soft_tnr_hrs"
      t.string  "emp_yet_to_attend"
      t.integer  "Knowledge_sessions_conducted"
      t.string  "employees_present_kss"
      t.integer  "certifications_done"
      t.string  "technology"
      t.integer  "year"
      t.integer "quater"

      t.timestamps null: false
    end

  end
  def down
  	drop_table :scorecards
  end
end
 