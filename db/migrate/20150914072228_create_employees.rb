class CreateEmployees < ActiveRecord::Migration
  def up
    create_table :employees,id: false do |t|
     t.primary_key "employee_id"
     t.integer "department_id"
     t.string "designation"
     t.string "Line_of_bussiness"
     t.integer "manager_id"
     t.string "name"
     t.string "status"
     t.string "location"
     t.date "date_of_joining"
     t.string "email_id"
    end
  end
  def down 
  drop_table :employees
  end

end
