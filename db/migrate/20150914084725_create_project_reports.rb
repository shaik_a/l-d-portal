class CreateProjectReports < ActiveRecord::Migration
  def up
    create_table :project_reports do |t|
    	t.integer "training_id"
      t.string "trainee_name"
      t.float "gui"
    	t.float "timeline"
    	t.float "time_space"
    	t.float "logic"
    	t.float "code_convention"
    	t.float "use_technology"
    	t.float "tools_usage"
    	t.float "presentation"
      t.timestamps null: false
    end
  end
  def down
  	drop_table :project_reports
  end
end
