class CreateScores < ActiveRecord::Migration
  def up
    create_table :scores do |t|
      t.float :gui
      t.float :timeline
      t.float :time_space
      t.float :logic
      t.float :code_convention
      t.float :use_technology
      t.float :tools_usage
      t.float :presentation
      t.float :Time_space_effeciency
      t.float :code_optimization
      t.float :meeting_timelines
      t.float :code_integrity
     t.float :knowledge
     t.float :format_submission


      t.timestamps null: false
    end
  end
   def down
    drop_table :scores
  end
end



