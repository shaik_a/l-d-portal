class CreateEvaluationReports < ActiveRecord::Migration
  def up
    create_table :evaluation_reports do |t|
     t.integer "training_id"
     t.string "trainee_name"
     t.string "tests"
     t.date "date_taken"
     t.float "marks_scored"
     t.float "max_marks"
    
     t.timestamps null: false
    end
    
  end
  def down
  	drop_table :evaluation_reports
  end
end
