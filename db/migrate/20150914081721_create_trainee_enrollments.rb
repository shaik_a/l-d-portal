class CreateTraineeEnrollments < ActiveRecord::Migration
  def up
    create_table :trainee_enrollments do |t|

      t.integer "employee_id" 
      t.string "trainee_name"
      t.integer "training_id"
      t.string  "cleared"
      t.timestamps null: false
    end
    add_index("trainee_enrollments","employee_id")
    add_index("trainee_enrollments","training_id")
  end
  def down
  	drop_table :trainee_enrollments
  end
end
