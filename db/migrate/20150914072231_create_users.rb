class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|

      t.integer "employee_id"
      t.string "username", :limit => 30	
      t.string "password"
      t.string "role_id", :limit => 25
      t.timestamps null: false
    end
    add_index("users", "employee_id")
  end
  def down

  	 drop_table :users
  end
end
