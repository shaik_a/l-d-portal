class CreateEmployeeReports < ActiveRecord::Migration
  def up
    create_table :employee_reports do |t|
    	t.integer "employee_id"
      t.string "employee_name"      
    	t.string "Certificate_Name"
    	t.string "Certification_code"
    	t.string "Team"
      t.integer "total_marks"
      t.integer "Marks_Scored"
    	t.date "Date_of_completion"
    	t.decimal "Amount"
    	t.integer "Commitment_Period"
      t.string "status"
      t.timestamps null: false
    end
  end
  def down
  	drop_table :employee_reports
  end
end
