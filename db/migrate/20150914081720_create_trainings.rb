class CreateTrainings < ActiveRecord::Migration
  def up
    create_table :trainings do |t|
    	t.string "course"
      t.string "Type"
      t.string "batchno" 
      t.integer "trainer_id"
      t.string "trainer_name" 
      t.integer "no_of_trainees"
      t.string "trainees" 
      t.string "venue" 
      t.string "status"
      t.date "from"
    	t.date "to"
       t.timestamps null: false
    end
  end
  def down
  	drop_table :trainings
  end
end
