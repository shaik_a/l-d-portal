class CreateTasks < ActiveRecord::Migration
  def up
    create_table :tasks do |t| 
      t.integer "assignee_id"
    	t.string "assignee"
    	t.string "task_title"
    	t.string "description"
    	t.date "from"
    	t.date "to"
    	t.string "priority"
      t.string "status"
      t.timestamps null: false
    end
  end
  def down
drop_table :tasks
  end
end
