class CreateObservationReports < ActiveRecord::Migration
  def up
    create_table :observation_reports do |t|
     t.integer "training_id" 
     t.string "trainee_name"
     t.decimal "technical_knowledge"
     t.decimal "coding_skills"
     t.decimal "logical_thinking_skills"
     t.decimal "time_management"
     t.decimal "attendance"
     t.decimal "approach_towards_learning"
     t.decimal "attitude_and_body_language"
     t.decimal "written_communication"
     t.decimal "spoken_communication"
     t.decimal "participation"
     t.string "Remarks"
     t.timestamps null: false
    end
  end
  def down
  	drop_table :observation_reports
  end
end
