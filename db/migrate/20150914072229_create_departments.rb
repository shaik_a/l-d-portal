class CreateDepartments < ActiveRecord::Migration
  def up
    create_table :departments do |t|
    	t.string "name"
    	t.integer  "manager_id"
      t.integer  "no_of_employees", limit: 4
      t.timestamps null: false
    end
    add_index :departments, :manager_id, :unique=>true
  end
  def down
  drop_table :departments
  end 
end
