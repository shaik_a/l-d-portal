class CreateAttendanceReports < ActiveRecord::Migration
  def up
    create_table :attendance_reports do |t|
    t.integer  "training_id",    limit: 4
    t.string   "topic",          limit: 255
    t.date     "date"
    t.string   "trainee_name",   limit: 255



    t.string  "presence"


    t.integer  "no_of_sessions", limit: 4
    t.string   "facilitator",    limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    end
  end
  def down

  	drop_table :attendance_reports
  end
end

