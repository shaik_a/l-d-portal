class CreateRoles < ActiveRecord::Migration
  def up
    create_table :roles,id: false do |t| 
      t.integer "role_id",primary_key: true
      t.string "name"
    end
  end
  def down
    drop_table :roles
  end
end
