if Role.all.count < 1
  Role.create!([
    {role_id: 1, name: "admin"},
    {role_id: 2, name: "trainer"},
    {role_id: 3, name: "manager"}
  ])
end


if User.all.count < 1
  User.create!([
    {employee_id: 594, role_id: 1, username: "admin",    password: "admin123", email: "admin@gmail.com", encrypted_password: "$2a$10$8go47H15TKZ8nowYP/WksedJGVnpqwQjUXdm/Z6ObTrvh5Af6pEAu", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 11, current_sign_in_at: "2015-09-29 17:17:08", last_sign_in_at: "2015-09-29 17:11:21", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1"},
    {employee_id: 551, role_id: 2, username: "trainer1", password: "password", email: "trainer_1@foo.bar", encrypted_password: "$2a$10$kkgtlVmlzkGl6lAPUlw3wevlIYBqw222zgMECB2KUErCbnDY.D5kq", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 3, current_sign_in_at: "2015-09-29 17:17:27", last_sign_in_at: "2015-09-29 17:06:15", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1"},
    {employee_id: 553, role_id: 2, username: "trainer2", password: "password", email: "trainer_2@foo.bar", encrypted_password: "$2a$10$1ULZR2nSPSYXQutgSwWsauAvbNJXwcMKxNbDO38MkVEvfHHuByZf6", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 3, current_sign_in_at: "2015-09-29 17:06:24", last_sign_in_at: "2015-09-29 15:46:29", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1"},
    {employee_id: 455, role_id: 2, username: "trainer3", password: "password", email: "trainer_3@foo.bar", encrypted_password: "$2a$10$lg9xxkBOxOncY928Vs5u7uI1TbHJNiGLXLVIJYzhi/WAQ2CgxRbfe", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 7, current_sign_in_at: "2015-09-29 17:03:46", last_sign_in_at: "2015-09-29 17:00:18", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1"}
  ])
end



Employee.create!([
  {employee_id: 5200, department_id: nil, manager_id: 455, name: "employee_1",status: "Permanent", location: "Bengaluru",date_of_joining: "2015-12-01",email_id: "satyam@gmail.com", },
  {employee_id: 448, department_id: nil, manager_id: 453, name: "employee_1",status: "Permanent", location: "Bengaluru",date_of_joining: "2012-12-01",email_id: "poornima@gmail.com", },
  {employee_id: 501, department_id: nil, manager_id: nil, name: "admin",status: "Permanent", location: "Bengaluru",date_of_joining: "2012-12-01", email_id: "poornima@gmail.com", },
  {employee_id: 553, department_id: 3, manager_id: 455, name: "employee_3",status: "Permanent", location: "Bengaluru", date_of_joining: "2015-12-01",email_id: "mayura@gmail.com", },
])

