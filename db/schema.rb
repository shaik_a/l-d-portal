# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2017033013144005) do

  create_table "assignment_reports", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.float    "training_id",           limit: 24
    t.string   "trainee_name"
    t.float    "Time_space_effeciency", limit: 24
    t.float    "code_convention",       limit: 24
    t.float    "code_optimization",     limit: 24
    t.float    "logic",                 limit: 24
    t.float    "meeting_timelines",     limit: 24
    t.float    "code_integrity",        limit: 24
    t.float    "knowledge",             limit: 24
    t.float    "format_submission",     limit: 24
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "attendance_reports", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "training_id"
    t.string   "topic"
    t.date     "date"
    t.string   "trainee_name"
    t.string   "presence"
    t.integer  "no_of_sessions"
    t.string   "facilitator"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "certifications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "employee_id"
    t.string   "employee_name"
    t.string   "Test"
    t.date     "Date"
    t.string   "Name"
    t.float    "Total_Score",    limit: 24
    t.float    "Score_achieved", limit: 24
    t.float    "Perentage",      limit: 24
    t.string   "Remarks"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "departments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "manager_id"
    t.integer  "no_of_employees"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["manager_id"], name: "index_departments_on_manager_id", unique: true, using: :btree
  end

  create_table "employee_reports", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "employee_id"
    t.string   "employee_name"
    t.string   "Certificate_Name"
    t.string   "Certification_code"
    t.string   "Team"
    t.integer  "total_marks"
    t.integer  "Marks_Scored"
    t.date     "Date_of_completion"
    t.decimal  "Amount",             precision: 10
    t.integer  "Commitment_Period"
    t.string   "status"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "employees", primary_key: "employee_id", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "department_id"
    t.string  "designation"
    t.string  "Line_of_bussiness"
    t.integer "manager_id"
    t.string  "name"
    t.string  "status"
    t.string  "location"
    t.date    "date_of_joining"
    t.string  "email_id"
  end

  create_table "evaluation_reports", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "training_id"
    t.string   "trainee_name"
    t.string   "tests"
    t.date     "date_taken"
    t.float    "marks_scored", limit: 24
    t.float    "max_marks",    limit: 24
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "observation_reports", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "training_id"
    t.string   "trainee_name"
    t.decimal  "technical_knowledge",        precision: 10
    t.decimal  "coding_skills",              precision: 10
    t.decimal  "logical_thinking_skills",    precision: 10
    t.decimal  "time_management",            precision: 10
    t.decimal  "attendance",                 precision: 10
    t.decimal  "approach_towards_learning",  precision: 10
    t.decimal  "attitude_and_body_language", precision: 10
    t.decimal  "written_communication",      precision: 10
    t.decimal  "spoken_communication",       precision: 10
    t.decimal  "participation",              precision: 10
    t.string   "Remarks"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  create_table "project_reports", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "training_id"
    t.string   "trainee_name"
    t.float    "gui",             limit: 24
    t.float    "timeline",        limit: 24
    t.float    "time_space",      limit: 24
    t.float    "logic",           limit: 24
    t.float    "code_convention", limit: 24
    t.float    "use_technology",  limit: 24
    t.float    "tools_usage",     limit: 24
    t.float    "presentation",    limit: 24
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "reports_trainings", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "trainee_enrollement_id"
    t.integer "attendance_report_id"
    t.integer "observation_report_id"
    t.integer "evaluation_report_id"
    t.integer "assignment_report_id"
    t.integer "project_report_id"
    t.integer "employee_report_id"
    t.integer "certification_id"
    t.index ["trainee_enrollement_id", "attendance_report_id", "observation_report_id", "evaluation_report_id", "assignment_report_id", "project_report_id", "employee_report_id", "certification_id"], name: "trainee_report_index", using: :btree
  end

  create_table "roles", primary_key: "role_id", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
  end

  create_table "scorecards", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "training_needs"
    t.string   "employees_present_tnr"
    t.integer  "employees_total"
    t.float    "tech_tnr_hrs",                 limit: 24
    t.float    "soft_tnr_hrs",                 limit: 24
    t.string   "emp_yet_to_attend"
    t.integer  "Knowledge_sessions_conducted"
    t.string   "employees_present_kss"
    t.integer  "certifications_done"
    t.string   "technology"
    t.integer  "year"
    t.integer  "quater"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "scores", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.float    "gui",                   limit: 24
    t.float    "timeline",              limit: 24
    t.float    "time_space",            limit: 24
    t.float    "logic",                 limit: 24
    t.float    "code_convention",       limit: 24
    t.float    "use_technology",        limit: 24
    t.float    "tools_usage",           limit: 24
    t.float    "presentation",          limit: 24
    t.float    "Time_space_effeciency", limit: 24
    t.float    "code_optimization",     limit: 24
    t.float    "meeting_timelines",     limit: 24
    t.float    "code_integrity",        limit: 24
    t.float    "knowledge",             limit: 24
    t.float    "format_submission",     limit: 24
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "tasks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "assignee_id"
    t.string   "assignee"
    t.string   "task_title"
    t.string   "description"
    t.date     "from"
    t.date     "to"
    t.string   "priority"
    t.string   "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "trainee_enrollments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "employee_id"
    t.string   "trainee_name"
    t.integer  "training_id"
    t.string   "cleared"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["employee_id"], name: "index_trainee_enrollments_on_employee_id", using: :btree
    t.index ["training_id"], name: "index_trainee_enrollments_on_training_id", using: :btree
  end

  create_table "trainings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "course"
    t.string   "Type"
    t.string   "batchno"
    t.integer  "trainer_id"
    t.string   "trainer_name"
    t.integer  "no_of_trainees"
    t.string   "trainees"
    t.string   "venue"
    t.string   "status"
    t.date     "from"
    t.date     "to"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "employee_id"
    t.string   "username",               limit: 30
    t.string   "password"
    t.string   "role_id",                limit: 25
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.string   "email",                             default: "", null: false
    t.string   "encrypted_password",                default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",                   default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["employee_id"], name: "index_users_on_employee_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree
  end

end
