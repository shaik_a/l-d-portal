Rails.application.routes.draw do
  resources  :trainings
  resources  :admins
  resources  :score_cards
  devise_for :users

  root to: 'users#index'
end
