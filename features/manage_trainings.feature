Feature: Manage trainings
	In order to create a training schedule
	As an admin and as a trainer
	I want to create, manage and view trainings

Scenario: training list
	Given I login as admin
	And I have training titled ROR for satyam
	And I have training titled ISMS for mayura
	When I go to trainings
	Then I should see "ROR"
	And I should see "ISMS"

Scenario: training list for trainer
	Given I login as trainer
	And I have training titled ROR for satyam
	And I have training titled ISMS for mayura
	Then I should see "ROR"
	And I should not see "ISMS" 	

Scenario: training list for Lob,director and managers (no trainees added)
	Given I login as director
	And I have training titled ROR for satyam
	Then I should not see "ROR"

Scenario: add a training
	Given I login as admin
	When I go to training#new
	And I should be able to add new training
	
