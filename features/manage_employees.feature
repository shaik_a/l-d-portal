Feature: Manage databases
	In order to add and maintain employee records
	As an admin 
	I want to create, manage and view employee records



Scenario: employee database
	Given I login as admin
	When I go to employees
	Then I should see "pranav"
	And I should see "arun"

Scenario: department database
	Given I login as admin
	When I go to departments
	Then I should see "WEB"
	And I should see "Engineering"


Scenario: add a employee
	Given I login as admin
	When I go to employees#new
	And I should be able to add new employee

Scenario: add a department
	Given I login as admin
	When I go to departments#new
	And I should be able to add new department	

Scenario: remove a employee
	Given I login as admin
	When I remove an employee(arun)
	And I go to employees
	Then I should not see "arun"

	Scenario: remove a department
	Given I login as admin
	When I remove an department(WEB)
	And I go to departments
	Then I should not see "WEB"