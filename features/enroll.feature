Feature: add, remove and view Trainees
	In order to add and remove trainees from a training
	As an admin and trainer 
	I want to add, remove and view trainees in a training


Scenario: add a trainee
	Given I login as admin
	When I go to trainings
	And I click on trainees
	Then I should be able to add new trainees

Scenario: add a trainee(by trainer)
	Given I login as trainer
	And I click on trainees
	Then I should not be able to add new trainees

Scenario: remove a trainee
	Given I login as admin
	And I click on trainees
	Then I should be able to remove trainees


Scenario: remove a trainee(by trainer)
	Given I login as trainer
	And I click on trainees
	Then I should not be able to remove a trainee	