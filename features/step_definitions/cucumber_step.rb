Given(/^I have training titled (.+) for satyam$/ ) do |courses|
  courses.split(', ').each do |course|
  	Training.create!(:course=>course,:Type=>'Foundation',:batchno=>5,:trainer_id=>551,:trainer_name=>"satyam",:venue=>"training room",:status=>"created",:from=>"22-06-2015",:to=>"22-06-2015")
	end
end

Given(/^I have training titled (.+) for mayura$/ ) do |courses|
  courses.split(', ').each do |course|
  	Training.create!(:course=>course,:Type=>'Lateral',:trainer_id=>553,:trainer_name=>"mayura",:venue=>"training room",:status=>"created",:from=>"22-06-2015",:to=>"22-06-2015")
	end
end

Given(/^I login as trainer$/) do
  visit new_trainer_session_path
  fill_in "trainer[username]", :with => "satyam"
  fill_in "trainer[password]", :with => "password"
  click_button "Log in"
end

Given(/^I login as admin$/) do
  visit new_admin_session_path
  fill_in "admin[username]", :with => "admin"
  fill_in "admin[password]", :with => "admin123"
  click_button "Log in"
end

Given(/^I login as director$/) do
 visit new_lob_session_path
  fill_in "lob[username]", :with => "pranav"
  fill_in "lob[password]", :with => "password"
  click_button "Log in"
end




#####################################################################################################

When /^(?:|I )go to (.+)$/ do |page_name|
  visit page_name
end

When(/^I should be able to add new training$/) do
  if defined?(Spec::Rails::Matchers)
    response.should contain("Create Training")
  end
end

When(/^I should be able to add new employee$/) do
  if defined?(Spec::Rails::Matchers)
    response.should contain("New Employee")
  end
end

When(/^I should be able to add new department$/) do
  if defined?(Spec::Rails::Matchers)
    response.should contain("Add New Department")
  end
end

When(/^I remove an employee\(arun\)$/) do
page.all('#table').each do |tr|
next unless tr.has_selector?('a[href*="/employees/512/delete"]')
click_link "fa fa-trash-o fa-2x"
click_link "commit"
end
end


When(/^I remove an department\(WEB\)$/) do
page.all('#table').each do |tr|
next unless tr.has_selector?('a[href*="/departments/4/delete"]')
click_link "fa fa-trash-o fa-2x"
click_link "commit"
end
end

When(/^I click on trainees$/) do
  page.all('#table').each do |tr|
click_link "fa fa-user fa-2x"
end
end


#####################################################################################################


Then(/^I should be able to add new trainees$/) do
  if defined?(Spec::Rails::Matchers)   
    response.should contain("add trainees")
  end
end

Then /^(?:|I )should see "([^\"]*)"$/ do |text|
  if defined?(Spec::Rails::Matchers)
    response.should contain(text)
  end
end

Then /^(?:|I )should not see "([^\"]*)"$/ do |text|
  if defined?(Spec::Rails::Matchers)
    response.should_not contain(text)
  
  end
end


Then(/^I should see that training$/) do
    if defined?(Spec::Rails::Matchers)
    response.should contain("Role of PC")
  end
end

Then(/^I should not be able to add new trainees$/) do
  if defined?(Spec::Rails::Matchers)
    response.should_not contain("add trainees") #the actual button to add trainees
  
  end
end

Then(/^I should be able to remove trainees$/) do
  page.all('#table').each do |tr|
next unless tr.has_selector?('a[href*="/tnrenrolls/2/delete?training_id=1"]')
click_link "fa fa-trash-o fa-2x"
click_link "commit"
end
end

Then(/^I should not be able to remove a trainee$/) do
 if defined?(Spec::Rails::Matchers)
    response.should_not contain("fa fa-trash-o fa-2x") 
  
  end
end
